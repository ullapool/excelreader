import pandas as pd
import openpyxl as oxl
import numpy as np
import matplotlib.pyplot as plt
import globals
import os
from tkinter import messagebox

#globals.sumExcel
class SumGenerator():

    @staticmethod
    def sum_generator():
        df_sum = pd.read_excel(globals.initialSumExcel)

        df_sortedTimeSpan = df_sum[~df_sum.Month.astype(str)
                            .str.contains('201903|201904|201905|201906|201907|201908|201909|201910|201911',
                            na = False)]
        df_sortedTimeSpan = df_sortedTimeSpan.rename(columns={'cf_Closed_By_Name':'Name', '# Task':'Task'})

        employeeList = df_sortedTimeSpan['Name'].tolist()

        for i in range(len(employeeList)):
            currentName = employeeList[i]
            if i == 0:
                df_first = (df_sortedTimeSpan.loc[(df_sortedTimeSpan['Name'] == currentName) & (df_sortedTimeSpan['f_Task_Type'] == 'FCO_KOPA')])
                df_first['Total'] = df_first.Task.sum()
                df_kopa = df_first
            else:
                currentName = employeeList[i]
                df_current = (df_sortedTimeSpan.loc[(df_sortedTimeSpan['Name'] == currentName) & (df_sortedTimeSpan['f_Task_Type'] == 'FCO_KOPA')])
                df_current['Total'] = df_current.Task.sum()
                df_kopa = df_kopa.append(df_current)
            #print("everything else", currentName)

    #following name is not listed in original excel file
    #therefor has to be added manualy -- later this can be used for user adding employees
    #themselfes using the GUI function.
        df_addName = {'Name':'some name', 'Total':50}
        df_kopa = df_kopa.append(df_addName, ignore_index=True)
        df_kopa = df_kopa.drop_duplicates('Name')
        df_kopa = df_kopa.sort_values(by=['Name'])
        df_kopa = df_kopa.reset_index(drop=True)
        fileName = "Total_KOPA_Pro_MA_V2.xlsx"

        df_kopa[['Name', 'Total']].to_excel(os.path.join(globals.saveFilePath,fileName))
        if globals.sumExcel != "":
            globals.status.config(text="*** Sum Excel generated! ***", bg='PaleGreen2')
        else:
            messagebox.showerror("Error", "Excel file could not get generated! Contact the developer.")
