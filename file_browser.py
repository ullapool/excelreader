#sys.path.append(".")
import tkinter as tk
from tkinter import *
from tkinter import ttk
import sys
from tkinter import filedialog
import os
import globals
from tkinter import messagebox

class FileBrowser():
    displayedName = ""

    @staticmethod
    def getStorePath(path, sumLabel):
        files = [('All Files', '*.*')]
        fileName = filedialog.askdirectory()
        globals.saveFilePath = fileName
        globals.status.config(text="Now get the <Sum Excel> your Teamleader has given you", bg="PaleGreen1")
        path.config(text=fileName)
        globals.sumExcel = globals.saveFilePath + "/Total_KOPA_Pro_MA_V2.xlsx"
        sumLabel.config(text=globals.sumExcel)


    @staticmethod
    def getFile(labelName,valNum):
        fileName = filedialog.askopenfilename()

        if valNum == 1:
            globals.initialSumExcel = fileName
        if valNum == 2:
            globals.mainExcel = fileName
            globals.status.config(text="Well done, one more to go!", bg="PaleGreen3")
        if valNum == 3:
            globals.previousConvertedExcel = fileName
            globals.status.config(text="Well done, all files have been loaded!", bg="PaleGreen3")
        if valNum == 4:
            globals.sumExcel = fileName

        displayedName = os.path.basename(fileName)
        labelName.config(text=displayedName)
