import tkinter as tk
from tkinter import *
from tkinter import ttk
import sys
from tkinter import filedialog
import os
from tkinter import messagebox
import globals
from sumGenerator import SumGenerator
from reportingGenerator import ReportingGen


class Validator():

    @staticmethod
    def sumValidator():
        if globals.saveFilePath == "":
            messagebox.showinfo('Information', 'Please choose where you want to store your files!')
            globals.status.config(text="*** Continue loading all nessecary files! ***")
        else:
            globals.status.config(text="*** Sum Excel is being generated ***")
            SumGenerator.sum_generator()

    @staticmethod
    def validate():
        if (globals.mainExcel and globals.previousConvertedExcel) == "":
            messagebox.showinfo('Information', 'Please select both mandatory files')

        elif globals.mainExcel == globals.previousConvertedExcel or globals.mainExcel == globals.sumExcel or globals.sumExcel == globals.previousConvertedExcel:
            messagebox.showerror("Error", "One or more files have the same filepath or name!")
        else:
            globals.status.config(text="*** Excel files are being generated ***")
            ReportingGen.repoGen()
