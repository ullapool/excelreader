from datetime import date
import pandas as pd
import openpyxl as oxl
import numpy as np
import matplotlib.pyplot as plt
from xlsxwriter.utility import xl_rowcol_to_cell
import os
import globals

class ReportingGen():

    @staticmethod
    def repoGen():
        df_load = pd.read_excel(globals.mainExcel)
        df_total = pd.read_excel(globals.sumExcel)
        df_c1_load = pd.read_excel(globals.previousConvertedExcel)

        ###rename clumns###
        #df_load.rename(columns={'this is gonna change to..' : 'this'}, inplace=True)
        today = date.today()
        today_converted = today.strftime("%b-%d-%Y")
        #df_load.loc[(df_load['Team']== 'CX-CFS-C-1') and (df_load['Team'] == 'CX-CBU-RUN-C-IMA')]
        df_c1 = df_load[df_load['Team'].isin(['CX-CFS-C-1', 'CX-CBU-RUN-C-IMA'])]
        df_c1 = df_c1[['Techniker', 'KW', 'Datum FCO', 'Limit 60 Tage', 'it_Incident_Id']]
        df_c1 = df_c1[~df_c1.Techniker.astype(str).str.contains('name1|name2|name3', na = False)]
        df_c1 = df_c1.sort_values(by=['Techniker'])
        #Formate Date Time
        df_c1['Datum FCO'] = pd.to_datetime(df_c1["Datum FCO"]).dt.strftime("%d-%m-%Y")
        df_c1= pd.concat([df_c1_load, df_c1], axis = 0)
        df_c1 = df_c1.drop_duplicates(subset = "it_Incident_Id")
        df_c1 = df_c1.reset_index(drop=True)

        df_c1 = df_c1.drop(columns=['Total'])
        df_c1 = pd.merge(df_c1, df_total, left_on='Techniker', right_on='Name')
        #df_c1 = pd.merge(df_c1, df_total[['Total', 'Name']], on='Total', how='right')
        df_c1= df_c1.drop(columns=['Name','Unnamed: 0'])


        ###adding math logic to table cloumns###
        df_c1['Berücksichtig Januar bis'] = today_converted
        df_c1['Fehler Total'] = df_c1['Techniker'].map(df_c1['Techniker'].value_counts())
        df_c1['Fehlerquote in %'] = (df_c1['Fehler Total'] * 100.0) / df_c1['Total']
        df_c1 = df_c1[['Techniker', 'KW', 'Datum FCO', 'Limit 60 Tage', 'it_Incident_Id', 'Quittierung CFS', 'Berücksichtig Januar bis', 'Total', 'Fehler Total','Fehlerquote in %']]
        decimals = pd.Series([2], index=['Fehlerquote in %'])
        df_c1 = df_c1.round(decimals)

        currentDate = today.strftime("%b-%Y")
        fileName = "C1_Reporting_" + currentDate + ".xlsx"
        output_file = os.path.join(globals.saveFilePath, fileName)
        df_c1.to_excel(output_file, index=False) #, index=False
        #df_c1.to_excel("C1_Reporting_" + date + ".xlsx", index=False)

        employeeList = df_c1['Techniker'].tolist()

        for i in range(len(employeeList)):
            currentEmployee = employeeList[i]
            df_currentEmployee = df_c1.loc[df_c1['Techniker'] == currentEmployee]
            employees_file_name = currentEmployee + "_" + currentDate + ".xlsx"
            employees_output_file = os.path.join(globals.saveFilePath, employees_file_name)
            df_currentEmployee.to_excel(employees_output_file, index=False)

        ###### Styling ######

        writer = pd.ExcelWriter(output_file, engine = 'xlsxwriter')
        df_c1.to_excel(writer, index = False)
        workbook = writer.book
        worksheet = writer.sheets['Sheet1']
        number_rows = len(df_c1.index)
        # Define our range for the color formatting
        color_range = "J2:J{}".format(number_rows+1)

        # Add a format. Light red fill with dark red text.
        format1 = workbook.add_format({'bg_color': '#FFC7CE',
                                       'font_color': '#9C0006'})

        # Add a format. Green fill with dark green text.
        format2 = workbook.add_format({'bg_color': '#C6EFCE',
                                       'font_color': '#006100'})


        # Highlight the top 5 values in Green
        worksheet.conditional_format(color_range, {'type': 'cell',
                                                   'criteria': '<=',
                                                   'value': 1.0 ,
                                                   'format': format2})
        # Highlight the bottom 5 values in Red
        worksheet.conditional_format(color_range, {'type': 'cell',
                                           'criteria': '>',
                                           'value': 1.0,
                                           'format': format1})
        writer.save()

        globals.status.config(text="*** All files are successfully generated. You may close this Window now. ***", bg='PaleGreen4')
