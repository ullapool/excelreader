import tkinter as tk
from tkinter import *
from tkinter import ttk
import sys
from tkinter import filedialog
import datetime
from datetime import date
import pandas as pd
import openpyxl as oxl
import numpy as np
import matplotlib.pyplot as plt
from xlsxwriter.utility import xl_rowcol_to_cell
import os
from file_browser import FileBrowser
from validation import Validator
from tkinter import messagebox
import globals

class Application:
    def __init__(self):
        self.root = tk.Tk()
        #self.root.geometry("600x400")
        self.root.title("Excelfile Converter")
        self.frame = tk.Frame(self.root, padx = 10, pady = 10)

        self.storeLabel = tk.Label(self.frame, width=35, height=1, borderwidth=2, relief="groove", text="", font=("Calibri Light", 14), bg='lightblue')
        self.sumLabel = tk.Label(self.frame, width=35, height=1, borderwidth=2, relief="groove", text="", font=("Calibri Light", 14), bg='lightblue')
        self.label1 = tk.Label(self.frame, width=35, height=1, borderwidth=2, relief="groove", text="", font=("Calibri Light", 14), bg='lightblue')
        self.label2 = tk.Label(self.frame, width=35, height=1, borderwidth=2, relief="groove", text="", font=("Calibri Light", 14), bg='lightblue')
        self.labelslabel = tk.Label(self.frame, width=15, height=1, borderwidth=1, text="Sum Excel", font=("Calibri Light", 14))
        self.label3 = tk.Label(self.frame, width=35, height=1, borderwidth=2, relief="groove", text="", font=("Calibri Light", 14), bg='lightblue')

        self.getStorePath = tk.Button(self.frame,width=15, text="Save to", command=lambda: FileBrowser.getStorePath(self.storeLabel, self.label3), font=("Calibri Light", 14))
        self.getSum = tk.Button(self.frame, width=15, text="Get Sum Excel", command=lambda: FileBrowser.getFile(self.sumLabel, 1), font=("Calibri Light", 14))
        self.createSum = tk.Button(self.frame, width=15, text="Create Sum File", command=Validator.sumValidator)
        #self.getSumExcel = tk.Button(self.frame,width=15, state=DISABLED, text="Get Sum File", command= lambda: FileBrowser.getFile(self.label3, 4), font=("Calibri Light", 12))
        self.getMainExcel = tk.Button(self.frame, width=15, text="Get Main Excel", command= lambda: FileBrowser.getFile(self.label1, 2), font=("Calibri Light", 14))
        self.getPreviousExcel = tk.Button(self.frame, width=15, text="Get Previous Excel", command= lambda: FileBrowser.getFile(self.label2, 3), font=("Calibri Light", 14))
        self.createButton = tk.Button(self.frame, width=15, text="Create", command=Validator.validate)

        self.getStorePath.grid(column=0, row=0, pady=10, padx=10)
        self.getSum.grid(column=0, row=1, pady=10, padx=10)
        self.createSum.grid(column=2, row=2, pady=10, stick=S)
        self.getMainExcel.grid(column=0, row=4, pady=10, padx=10)
        self.getPreviousExcel.grid(column=0, row=5, pady=10, padx=10)
        self.labelslabel.grid(column=0, row=6, pady=10, padx=10)
        #self.getSumExcel.grid(column=0, row=6, pady=10, padx=10)
        self.createButton.grid(column=2, row=9, pady=20, stick=S)

        self.storeLabel.grid(column=2, row=0, pady=10, columnspan=2)
        self.sumLabel.grid(column=2, row=1, pady=10, columnspan=2)

        self.label1.grid(column=2, row=4, pady=10, columnspan=2)
        self.label2.grid(column=2, row=5, pady=10, columnspan=2)
        self.label3.grid( column=2, row=6, pady=10, columnspan=2)

        self.frame.pack()#(row=0, column=0, sticky="nesw")

        globals.status = tk.Label( text="*** Choose location where you want to store all generated Excel files ***", bd=1, bg="#A9A9A9", relief=SUNKEN, anchor=S )
        globals.status.pack(side=BOTTOM, fill=X)
        self.root.mainloop()


app = Application()
